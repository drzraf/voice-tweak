# Voice-tweak

GUI for Sox and pulseaudio allowing for real time voice distortion

## Requirements
- Python 3.5 or later
- SoX (binary)
- PulseAudio
